<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Xiliari_model extends CI_Model {

    public function get_admin_data($admin_id)
    {
        $sql = "SELECT * "
        .  "FROM  `list_admin` "
        .  "WHERE `admin_id` = ? ;";
        $query = $this->db->query($sql,[$admin_id]);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
        
    }
    public function get_permission($level = FALSE)
    {
     $id = $this->session->userdata('id');
     $admin = $this->get_admin_data($id);
     if ($admin->level_admin_id <= $level) {
        return TRUE;
     } else {
         return FALSE;
     }
     
    }
    public function get_list_admin()
    {
        $list = [];
        $sql = "SELECT * FROM `list_admin` la JOIN `level_admin` lad ON la.`level_admin_id` = lad.`level_admin_id` JOIN  `status` st ON la.`status_id` = st.`status_id`  WHERE la.`status_id`  IN (1,2,3,4) ";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->result();
        } else {
            return FALSE;
        }
        
    }
    public function get_app_list()
    {
       $sql = "SELECT * FROM `list_admin` WHERE `status_id` = 5 ";
       $query = $this->db->query($sql);
       if ($query) {
           return $query->result();
       } else {
           return FALSE;
       }
       
    }
    public function accept_apps($admin_id)
    {   $response = new stdClass();
        $response->success = FALSE;
        $response->message = "Failed, Something gones wrong ";
        $sql = "UPDATE `list_admin` SET `status_id` = 4 WHERE `admin_id` = ? ;";
        $query = $this->db->query($sql,[$admin_id]);
        if ($query) {
            $token = strtoupper($this->generate_token());
            $sql2 = "INSERT INTO `admin_token` (`admin_id`, `token`) VALUES (?, ?) ;";
            $query2 = $this->db->query($sql2,[$admin_id,$token]);
            if ($query2) {
                $email = $this->get_admin_data($admin_id)->email;
                $status = "confirmation";
                $email = $this->send_email($email,$status,$token);
                if ($email) {
                    $response->success = TRUE;
                    $response->message = "Accept admin success";
                    $response->link = "view_application_list()";
                } else {
                    $response->message = "Failed sending email please repeat the step";
                }
                
            } else {
              $resnponse->message = "Failed generate token";   
            }
        } else {
            $response->message = "Failed your data seem not valid";
        }
        return $response;
        
    }
    public function generate_token()
    {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $length = strlen($characters);
        $random_string = "";
        for ($i = 0; $i < 16; $i++) {
            $random_string .= $characters[rand(0, $length - 1)];
        }
        return $random_string;
    }
    public function send_email($email,$status,$token = FALSE)
    {   
        if ($status === "registration") {
           $subject = "Registrasion";
           $template = $this->load->view('email_register','', TRUE);
        } else if($status === "confirmation") {
            $subject = "Reconfirmation";
            $data['token'] = $token;
            $template = $this->load->view('email_confirmation', $data,TRUE) ;
        } else{
            $subject = " ";
        }
        // $data = "test";
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "huntzrahmadi@gmail.com";
        $config['smtp_pass'] = "farhanrahmadi";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $ci->email->initialize($config);
        $ci->email->from('huntzrahmadi@gmail.com', 'Farhan Rahmadi');
        $list = array($email);
        $ci->email->to($list);
        $ci->email->subject($subject);
        $ci->email->message($template);
        // if ($this->email->send()) { uncomment if you want to send email
        if(TRUE){
            return TRUE;
        } else {
            return FALSE;
        }
        
    }

    public function get_admin_detail($id)
    {
        $sql = "SELECT * ".
        "FROM `list_admin` la ".
        "JOIN `level_admin` lad ".
        "ON la.`level_admin_id` = lad.`level_admin_id` ".
        "JOIN  `status` st ".
        "ON la.`status_id` = st.`status_id`  ".
        "WHERE la.`admin_id` = ? ";
        $query = $this->db->query($sql,[$id]);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return $this->db->last_query();
        }
        
    }

    public function get_admin_activity($id)
    {
        $sql = "SELECT *".
        "FROM `log_activity` la ".
        "JOIN `list_activity` lia ".
        "WHERE la.`admin_id` = ? ";
        $query = $this->db->query($sql,[$id]);
        if ($query) {
            return $query->result();
        } else {
            return $this->db->last_query();
        }
        
    }

    public function get_admin_level_list()
    {
        $sql = "SELECT *".
        "FROM `level_admin` ";
        $query = $this->db->query($sql);
        if ($query) {
            return $query->result();
        } else {
            return $this->db->last_query();
        }
        
    }

}

/* End of file xiliari_model.php */
/* Location: ./application/models/xiliari_model.php */