<div class="fluid-container">
  <div class="container">
    <h2>Edit Profile</h2>
    <form id="primaryForm">
      <div class="grid-container column-2">
        <div class="form-group">
          <label for="admin_name">Admin Name</label>
          <input type="text" class="form-control" value="<?php echo $admin_detail->admin_name ?>" name="admin_name">
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" class="form-control" value="<?php echo $admin_detail->username ?>" name="username">
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" id="email" value="<?php echo $admin_detail->email ?>" name="email">
        </div>
        <div class="form-group">
          <label for="admin_level">Select list:</label>
          <select class="form-control" name="admin_level">
            <?php 
              foreach ($level_list as $level) {
                if ($level->level_admin_id == $admin_detail->level_admin_id) {
                  echo "<option value='$level->level_admin_id' selected >$level->level_name</option>";
                } else {
                  echo "<option value='$level->level_admin_id'>$level->level_name </option>";
                }
              }

             ?>
          </select>
        </div>
        <div class="form-group">
          <label for="pwd">Password:</label>
          <input id="password" type="password" class="form-control" name="password">
          <small>leave emty if no change</small>
        </div>
        <div class="form-group">
          <label for="pwd">Password:</label>
          <input id="passconf" type="password" onkeydown="passCheck()" class="form-control" name="passconf">
          <small id="warning"></small>
        </div>
      </div>
      <div id="submitButton" onclick="xSubmitForm('primaryForm','process_admin_edit')" type="submit" class="btn btn-default">Submit</div>
    </form>
  </div>
</div>
<script>
  function passCheck() {
          var passconf = document.getElementById('passconf');
          var password = document.getElementById('password');
          var warn = document.getElementById('warning');
          console.log(password.value+" "+passconf.value);
          if (passconf.value != password.value) {
            passconf.classList.add("input-warning");
            warn.innerHTML = "Password didn't match";
            document.getElementById('submitButton').disabled = true;

          } else {
            passconf.classList.remove("input-warning");
            warn.innerHTML = "";
            document.getElementById('submitButton').disabled = false;
          }
          
        }
</script>
<script>
  function xSubmitForm(formID, targetPage, requireConfirmation, confirmMessage) {
    var confirmation = false;
    var is_valid = true;
    var error_message = "";
    var identic_value = "";
    if (requireConfirmation == true) {
        if (confirmMessage == "" || confirmMessage == null) {
            confirmMessage = "Are you sure you want to perform this action?";
        }
        confirmation = confirm(confirmMessage);
    } else {
        confirmation = true;
    }
    if (confirmation == true) {
            var targetURL = base_url + targetPage;
            var form = document.getElementById(formID);
            var input = new FormData(form);
            input.append("ajax", "true");
            xiliariContent.append("<div class='gs-load-overlay'></div>");
            if (targetPage != "") {
                $.ajax({
                    url: targetURL,
                    type: "POST",
                    data: input,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (result) {
                         $("#xWrapper").html(result);
                    },
                    error: function () {
                        xNavigate("error_page");
                    }
                });
            }
        }
    }
</script>