<?php 
	if(isset($message)){
		echo $message;
	}
 ?>
<div class="container-fluid">
	<h2>Application List</h2>
	<div class="container" style="margin-top: 20px">
		<table class="table table-striped">
		<thead>
			<th>Username</th>
			<th>Email</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php foreach ($app_list as $list) { ?>			
			<tr>
				<td><?php echo $list->username ?></td>
				<td><?php echo $list->email ?></td>
				<td><button class="btn btn-success" onclick="xNavigate('process_admin_accept?id=<?php echo $list->admin_id ?>')"> Accept</button> <buton onclick="xNavigate('process_admin_decline?id=<?php echo $list->admin_id ?>',true)" class="btn btn-danger"> Decline</buton></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	</div>
</div>