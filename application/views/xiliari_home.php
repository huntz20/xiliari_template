<!DOCTYPE html>
<html lang="in">
<head>
  <title>Xiliari Studio</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/xiliari-logo.png') ?>">
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Serif" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/xiliari.css') ?>">
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
</head>
<body>
  <!-- navbar start -->
  <nav class="navbar navbar-expand-md navbar-dark">
    <span class="brand-logo" onclick="xNavigate('view_dasboard')">Xiliari Studio</span>
    <div class="btn-signout"><button class="btn btn-default">Sign Out </button></div> 
  </nav>
    
    <!-- sidebar -->
   <div class="container-fluid no-padding no-margin">
     <div class="row no-padding no-margin">
       <div class="col-md-2 no-padding no-margin">
          <div class="sidebar">
          <div class="admin-info">
              <div class="admin-img-container">                
              <img src="<?php echo base_url('assets/images/admin_photo/default.png') ?>" alt="admin-photo" class="admin-img">
              </div><br>
              <span class="admin-greeting text-center">Welcome Huntz</span>
          </div>
        <div class="menu">
          <ul>
            <li onclick="xNavigate('view_dasboard')"><i class="fas fa-tachometer-alt" style="color: gray"></i> Dashboard</li>
            <li onclick="xNavigate('view_admin')"><i class="fas fa-users" style="color: gray"></i> Admin</li>
          </ul>                
        </div>
      </div>
       </div>
       <div class="col-md-10 no-padding no-margin">
         <div id="xWrapper" class="xWrapper">
           <div id="data">
           </div>
         </div>
       </div>
     </div>
   </div>

</body>
<script src="<?php echo base_url('assets/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
<script src="<?php echo base_url('assets/js/xiliari.js'); ?>"></script>
<script>
  $(document).ready(function () {
                xNavigate('view_dasboard');
            });
</script>
</html>