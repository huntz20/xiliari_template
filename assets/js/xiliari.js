  var base_url = "http://localhost/xiliari/xiliari/";
  var xiliariContent = $("div #data");


  function xNavigate(targetPage, requireConfirmation, confirmationMessage) {
    var corfirmation = false;
    if (requireConfirmation == true) {
      if (confirmationMessage == "" || confirmationMessage == null ) {
          confirmationMessage = "Are you sure want perform this action";
        }
        confirmation = confirm(confirmationMessage);
        } else {
          confirmation = true;
        }
      if (confirmation == true) {
          var targetURL = base_url + targetPage;
          xiliariContent.append("<div class='xiliari-overlay-load'></div>");
          if (targetPage != " " ) {
              $.ajax({
                url : targetURL,
                type : "POST",
                data: {
                  ajax: true
                },
                success: function (result) {
                  $("#xWrapper").html(result);

                },
                error: function (result) {
                  xNavigate('error_page');
                }
                
              })
          } 
      } 
  }

    function deleteSnackbar() {
      setTimeout(function () {
        $("#snackbar").remove();
      }, 3000);
    }
